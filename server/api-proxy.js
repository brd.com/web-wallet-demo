const SHOULD_LOG = true;

//import proxy from 'express-http-proxy';
const unet = require('unet');

const REQUEST_HEADERS = ['accept','accept-language','content-type','content-length','content-encoding','date','authorization'];
const RESPONSE_HEADERS = ['cf-ray','date','content-type','content-length','content-encoding'];

function filterHeaders(headers,list) {
  const r = {};

  for(var k in headers) {
    if(list.includes(k.toLowerCase())) {
      r[k] = headers[k];
    } 
  }

  return r;
}

async function streamToBuffer(stream) {
  return new Promise((resolve,reject) => {
    var bufs = [];
    stream.on('data', function(d){ bufs.push(d); });
    stream.on('end', function(){
      var buf = Buffer.concat(bufs);
      resolve(buf);
    });
    stream.on('error',function(error) {
      reject(error);
    });
  });
}

module.exports = async (req,res) => {
  function log(...args) {
    if(SHOULD_LOG) {
      req.console.log(...args);
    }
  }

  const headers = filterHeaders(req.headers,REQUEST_HEADERS);
  
  log(`=> ${req.method} ${req.url} (${JSON.stringify(headers)})`);
  let body = undefined;
  
  if(req.method.toLowerCase() != 'get') {
    if(SHOULD_LOG) {
      body = await streamToBuffer(req);
    } else {
      body = req;
    }
  }

  if(body) {
    log(`=> ${body.toString('utf-8')}`);
  }
  
  const inner = await unet({ baseURL: 'https://api.blockset.com/',
                             method: req.method,
                             url: req.url,
                             headers,
                             body: body,
                             middleware: [],
                           });

  const resHeaders = filterHeaders(inner.headers,RESPONSE_HEADERS);

  log(`<= ${inner.status} ${JSON.stringify(resHeaders)}`);

  let responseBody;
  if(SHOULD_LOG) {
    responseBody = await inner.body();
    log(`<= ${responseBody.toString('utf-8')}`);
  }

  res.status(inner.status);

  res.set(resHeaders);
  res.set('x-date',res.get('date'));
  
  res.set('x-response-id',req.id);
  
  if(responseBody) res.write(responseBody);
  else inner.pipe(res);
  res.end();
}
