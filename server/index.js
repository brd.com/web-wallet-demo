const express = require('express');
const app = express();
const apiProxy = require('./api-proxy');
                    
app.use('/api',apiProxy);

module.exports = app;
