export const pause = (delay) => new Promise(res => setTimeout(res,delay));

export function get(object,string) {
  const parts = string.split('.');
  return parts.reduce((o,key) => o&&o[key],object);
}

export function PromiseLatch() {
  this._res = [];
  this._rej = [];
}

PromiseLatch.prototype = {
  promise: function() {
    return new Promise((res,rej) => {
      this._res.push(res);
      this._rej.push(rej);
    });
  },
  resolve: function(...args) {
    if (this._res) {
      var call = this._res;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c(...args));
    }
  },
  reject: function() {
    var args = Array.prototype.slice.call(arguments);
    if (this._rej) {
      var call = this._rej;
      this._rej = [];
      this._res = [];

      call.forEach((c) => c.apply(this,args));
    }
  },
}

export function inheritProp(name) {
  return {
    inherited: true,
    get: function() {
      var p = this.$parent;
      while (p) {
        // if it has something, and it's not a computed property:
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name];
        }

        p = p.$parent;
      }

      return null;
    },
    set: function(value) {
      var p = this.$parent;

      while (p) {
        if (name in p && !(p.$options.computed && name in p.$options.computed && p.$options.computed[name].inherited == true)) {
          return p[name] = value;
        }

        p = p.$parent;
      }

      return null;
    },
  }
};

export function inPlaceFilter(array,callback) {
  let i = 0, j = 0;

  while (i < array.length) {
    const val = array[i];
    if (callback(val, i, array)) array[j++] = val;
    else console.log("Tossing ",i,val);
    
    i++;
  }

  array.length = j;
  return array;
}

/**
   The idea behind "worker" functions is to keep running repeatedly
   until it completes a full pass without getting re-invoked.
   Designed to be methods on a Vue instance.
*/

export function worker(key,cb) {
  let callback;
  
  if(cb) callback = cb;
  else {
    callback = key;
    key = "_worker";
  }

  if(typeof callback != 'function') {
    throw new Error("Worker callbacks must be functions");
  }
  
  return async function() {
    this[`${key}Dirty`] = true;

    if(this[`${key}Working`]) {
      return false;
    }
    
    this[`${key}Working`] = true;

    const nextTick = () => new Promise(resolve => this.$nextTick(resolve));

    await nextTick();
    await nextTick();
    await nextTick();
    
    while(this[`${key}Dirty`]) {
      this[`${key}Dirty`] = false;

      try {
        await callback.call(this);
      } catch(x) {
        this[`${key}Working`] = false;
        throw x;
      }
    }

    this[`${key}Working`] = false;
  }
}

