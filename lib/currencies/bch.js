import Vue from 'vue';

import shared from './_shared';
import utxo from './_utxo';

const bch = {
  computed: {
    
  }
};

export default function(wallet) {
  return new Vue({
    mixins: [shared,utxo,bch],
    computed: {
      wallet() { return wallet; }
    }
  });
}
