import HalStream from '../hal-stream';
import { get, worker } from '../utils';

import BN from 'bignumber.js';

export default {
  created() {
  },
  data() {
    return {
      initialized: false,

      blockHeight: 0,

      changeAddresses: [],
      receiveAddresses: [],
      watchedAddresses: [],

      keysByAddress: {},

      transactions: [],
      transactionsByID: {},
    };
  },
  methods: {
    addTransaction(transaction) {
      if(!transaction) return;
      
      const id = transaction.transaction_id;
      if(id in this.transactionsByID) return;

      this.transactionsByID[id] = transaction;
      this.transactions.push(transaction);
      this.$emit('transaction',transaction);

      // always assume we don't have a complete final block, check the last known block as well:
      if(transaction.block_height)
        this.blockHeight = Math.max(transaction.block_height-1,this.blockHeight);
    },

    isWatched(address) {
      return this.watchedAddresses.includes(address);
    },

    sync: worker(async function() {
      await this.updateWatchedAddresses();
      
      const addresses = await this.watchedAddresses;
      const blockchainId = this.blockchainId;
      const BATCH_SIZE = 10;

      const batches = [];
      
      for(let i = 0 ; i < addresses.length / BATCH_SIZE ; i++) {
        const start = i*BATCH_SIZE;
        const end = start + BATCH_SIZE;
        batches.push(addresses.slice(start,end));
      }

      const urls = batches.map(a => `transactions?max_page_size=20&include_raw=true&start_height=${this.blockHeight}&blockchain_id=${this.blockchainId}&address=${a.join('%2C')}`);

      const stream = new HalStream(urls,this.wallet.blockset.net,(error,result) => {
        if(error) {
          console.error(error);
        }
        if(result) {
          const txs = get(result,'_embedded.transactions');
          if(txs) {
            txs.forEach(this.addTransaction);
          }
        }
      });

      await stream.whenDone();

      this.$emit('sync-complete');

      this.initialized = true;
    }),

    postTransaction({ id, body }) {
      return this.wallet.blockset.net({
        method: 'POST',
        url: 'transactions',
        body: {
          transaction_id: `${this.blockchainId}:${id}`,
          blockchain_id: this.blockchainId,
          data: body.toString('base64')
        }
      });
      
      
    }
  }
}

