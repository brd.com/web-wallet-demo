import Vue from 'vue';

import shared from './_shared';
import utxo from './_utxo';

import Bitcore from 'bitcore-lib';

const btc = {
  computed: {
    blockchainId() {
      return `bitcoin${this.wallet.testnet?'-testnet':''}`;
    },
    currencyId() {
      return this.blockchainId + ":__native__";
    },
  },

  created() {
/*    this.$on('transaction',(tx) => {
      const B = Bitcore;
      const inbound = new Bitcore.Transaction(Buffer.from(tx.raw,'base64'));
      
      const outputs = tx._embedded.transfers.filter(t => t.to_address != 'unknown')
            .map(t => toOutput(inbound,t));
    }); */
  },

  methods: {
    toOutput(transfer) {
      const outputIndex  = Number(transfer.meta.n);
      const transaction = this.transactionsByID[transfer.transaction_id];
      const bitcoreTx = new Bitcore.Transaction(Buffer.from(transaction.raw,'base64'));
            
      // this doesn't work: transfer.meta['scriptPubKey.asm'];
      const script = bitcoreTx.outputs[outputIndex].script; 
      const address = transfer.meta['scriptPubKey.addresses'];

      const txId = bitcoreTx.id // transfer.meta['scriptPubKey.hex'];
      
      return new Bitcore.Transaction.UnspentOutput({
        txId: txId,
        outputIndex: outputIndex,
        address: address,
        script: script.toBuffer(),
        satoshis: Number(transfer.amount.amount),
      });
    },

    formattedAddress(key) {
      return key.publicKey.toAddress(this.wallet.testnet?'testnet':'mainnet').toString();
    },

    updateWatchedAddresses() {
      // main chain:
      this.receiveAddresses = this.addressChain(`m/0'/0`);
      // change addresses:
      this.changeAddresses = this.addressChain(`m/0'/1`).concat(this.addressChain(`m/0'/0/1`));

      this.alternateReceiveAddresses = this.addressChain(`m/0'/0/0`);

      // combos:
      this.watchedAddresses = this.receiveAddresses.concat(this.alternateReceiveAddresses,this.changeAddresses);
    },


    async send(address,amount,fee) {
      const unspent = this.unspentTransfers();
      const keys = unspent.map(u => this.keysByAddress[u.to_address].privateKey)
      
      var tx = new Bitcore.Transaction()
          .from(unspent.map(this.toOutput))
          .to([{address: address, satoshis: Number(amount)}])
          .change(this.changeAddress)
          .fee(fee)
          .sign(keys);
      // const size = tx.toBuffer().length;
      // tx = tx.fee(size * fee);

      return await this.postTransaction({ id: tx.hash, body: tx.toBuffer() });
    },
    
  },

  
  
};

export default function(wallet) {
  return new Vue({
    mixins: [shared,utxo,btc],
    computed: {
      wallet() { return wallet; }
    }
  });
}

