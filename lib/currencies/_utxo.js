import { get, inPlaceFilter } from '../utils';
import BN from 'bignumber.js';

export default {
  created() {
    this.$on('transaction',this._transactionAdded);
    this.$on('sync-complete',this.updateReceiveAddress);
  },
  data() {
    return {
      receiveAddress: null,
      balance: new BN(0),
    }
  },
  methods: {
    unspentTransfers() {
      const transfers = this.transactions.map(tr => tr._embedded.transfers).reduce((a,b) => a.concat(b),[]);
      const ourOutputs = transfers.filter(tr => this.isWatched(tr.to_address));
      const ourSpends = transfers.filter(tr => this.isWatched(tr.from_address));
      
      const unspentOutputs = ourOutputs.filter(out => !ourSpends.find(spent => {
        if(spent.from_address == out.to_address) {
          return out.transaction_id == `${this.blockchainId}:${spent.meta.txid}`;
        }
        return false;
      }));

      return unspentOutputs;
    },
    _transactionAdded(tx) {
      this.updateReceiveAddress();

      for(let tr of tx._embedded.transfers) {
        if(this.isWatched(tr.to_address)) this.balance = this.balance.plus(tr.amount.amount);
        if(this.isWatched(tr.from_address)) this.balance = this.balance.minus(tr.amount.amount);
      }
    },
    updateReceiveAddress() {
      this.updateWatchedAddresses();
      
      this.receiveAddress = this.receiveAddresses.find(a => !this.addressUsed(a));
      this.changeAddress = this.changeAddresses.find(a => !this.addressUsed(a));
    },
    addressUsed(address) {
      for(let transactions of this.transactions) {
        for(let transfers of get(transactions,'_embedded.transfers') || []) {
          if(transfers.to_address == address) return true;
        }
      }
      return false;
    },
    addressChain(path) {
      let base = this.wallet.deriveKey(path);
      let count = 20;
      const addresses = [];
      
      for(var i = 0 ; i < count ; i++) {
        const key = base.deriveChild(i);
        const address = this.formattedAddress(key);

        this.keysByAddress[address] = key;

        if(this.addressUsed(address)) count++;
        
        addresses.push(address);
      }
      
      return addresses;
    },
  }
};
