import Vue from 'vue';
import Mnemonic from 'bitcore-mnemonic';
import BlocksetAPI from './blockset-api';

import btc from './currencies/btc';
import bch from './currencies/bch';

// For demo purposes we have embedded the client token in the code
// here.  They are a long-lived token, so it's not a concern that it
// might expire.  However, they do allow people to make requests to
// the Blockset API and use your bandwidth.  If you have an
// environment where this key is possible to hide (i.e. a mobile app),
// it may make sense to do so:
const CLIENT_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI1ZDIzYWQ2Ni05YmE0LTRkYzEtYWY5OC02NzI1OTEzYTc2MGUiLCJicmQ6Y3QiOiJjbGkiLCJleHAiOjkyMjMzNzIwMzY4NTQ3NzUsImlhdCI6MTU3ODA2MzQxNH0.PS7CZowZ513NXkfMUoVvt6AWXaEHdFjEUESqwgstWLlpqbc2HiWNydpqzs9DXFqnw-HGbKpPhCihbng3fpQ2sA'

const proto = {
  data() {
    return {
      initialized: false,
      error: null,
      
      currencies: {},
      blockset: null,
    };
  },
  
  async created() {
    this.$set(this.currencies,'btc',btc(this));
    
    // this.$set(this.currencies,'bch',bch(this));

    this.blockset = new BlocksetAPI({
      clientJWT: CLIENT_TOKEN,
      key: this.deriveKey(`m/1'/0`),
      apiBase: this.apiBase,
    });

    const { result, error } = await this.blockset.ensureUserJWT();
    if(error) this.error = error;

    this._syncInterval = setInterval(this.sync,5000);
  },
  beforeDestroy() {
    clearInterval(this._syncInterval);
  },
  computed: {
    HDPrivateKey() {
      if(!this.mnemonic) return null;
      
      const m = new Mnemonic(this.mnemonic);
      
      return m.toHDPrivateKey();
    },
  },
  methods: {
    async sync() {
      for(let k in this.currencies) {
        await this.currencies[k].sync();
      }
    },
    deriveKey(path) {
      // TODO: This is a slow(ish) operation, probably a good idea to
      // memo-ize a lot of it later.
      return this.HDPrivateKey.deriveChild(path)
    },
  }
};

export default function(mnemonic,options={}) {
  const { testnet, api } = { testnet: true, ...options };
  
  return new Vue({
    mixins: [proto],
    computed: {
      mnemonic() { return mnemonic || (new Mnemonic(Mnemonic.Words.ENGLISH)).toString(); },
      testnet() { return testnet; },
      apiBase() { return api },
    }
  });
}
