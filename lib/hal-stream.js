import { get, PromiseLatch } from './utils';

export default class HalStream {
  constructor(urls,net,callback) {
    this.urls = urls;
    this.net = net;
    this._callback = callback;
    this.concurrency = 4;

    this.active = [];

    this.latch = new PromiseLatch();
    
    this.go();
  }

  whenDone() {
    return this.latch.promise();
  }

  stop() {
    this.stopped = true;
    
    this._callback = null;
    this.urls = null;
  }

  push(url) {
    this.urls.push(url);
    this.go();
  }

  finish(item) {
    const i = this.active.findIndex(o => o == item);
    this.active.splice(i,1);
    
    this.go();
  }
  
  
  async workOn(o) {
    this.active.push(o);

    const url = o.url || o;
    const retries = o.retries || 0;
    
    const { result, error, response } = await this.net(url);

    if(error) {
      if(retries > 3) {
        this.callback(error,response);
      } else {
        this.push({ url, retries: retries+1 });
      }
    } else {
      const next = get(result,'_links.next.href');
      
      if(next) {
        this.push(next.replace(/https?:\/\/.*?\//,''));
      }        

      this.callback(null,result);
    }

    this.finish(o);
  }
    
  done() {
    this.latch.resolve('');
    this.callback(null,null);
  }

  callback(error,result) {
    try {
      if(error) {
        this.latch.reject(error);
      }
      
      if(this._callback) this._callback(error,result);
    } catch(x) {
      console.error("Error during callback: ",x);
    }
  }

  async go() {
    if(this.active.length == 0 && this.urls.length == 0) {
      this.done();
      return;
    }
    
    while(this.active.length < this.concurrency && this.urls.length > 0) {
      const o = this.urls.shift()
      this.workOn(o);
    }
  }
}
