import WebSocket from 'ws';

export default class WebhookCatcher {
  constructor(channel) {
    this.channel = channel;
    this.url = `https://blockset.com/webhooks/${channel}`;
    this.ws = new WebSocket('wss://blockset.com/webhooks/ws');

    this.messages = [];
    this.handlers = [];
    
    this.ws.on('open',() => {
      this.ws.send(JSON.stringify({ type: 'listen', payload: { channel: channel } }));
    });
    
    this.ws.on('message',(msg) => {
      if(msg.type == 'pong') return;
      
      this.messages.push(msg);
      this.handle(msg);
    });

    this._interval = setInterval(() => this.ws.send('ping'),5000);
  }

  close() {
    clearInterval(this._interval);
    this.ws.close();
    
    delete this.ws;
  }

  handle(msg) {
    this.handlers.forEach(h => h(msg));
  }

  on(handler) {
    this.handlers.push(handler);
  }

  off(handler) {
    let i = this.handlers.indexOf(handler);
    if(i > -1) this.handlers.splice(i,1);
  }
  
  matching(callback) {
    return new Promise((resolve) => {
      const handler = (msg) => {
        if(callback(msg)) {
          resolve(msg);
          this.off(handler)
        }
      }

      try {
        for(var msg of this.messages) {
          if(callback(msg)) {
            resolve(msg);
            return;
          }
        }
        
        this.on(handler);
      } catch(x) {
        reject(x);
        this.off(handler);
      } 
    })
  }
  
}
