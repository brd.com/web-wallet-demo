const unet = require('unet');
const bitcore = require('bitcore-lib');
const Mnemonic = require('bitcore-mnemonic');
const { PromiseLatch } = require('./utils');

const ECDSASigFormatter = require('ecdsa-sig-formatter');

const USER_TOPICS = ['blockchains','blocks','transactions','transfers','subscriptions'];

class BlocksetAPI {
  // key is the cryptographic key to be used when communicating with
  // Blockset.
  //  
  // token is to authenticate with Blockset, either a "client" token,
  // in which case a user token will be created, or a "user" token,
  // which will be used (refreshing as necessary):
  constructor({ key, deviceId, clientJWT, userJWT, apiBase }) {
    this.net = this.net.bind(this);

    this.apiBase = apiBase;
    this.publicKey = key.publicKey;
    this.privateKey = key.privateKey;
    this.deviceId = deviceId || UUID();

    this.clientJWT = clientJWT;
    this.userJWT = userJWT;

    return;

    // TODO: Validate stuff, using somethign like below:
    const tokenParts = token.split('.');
    if(tokenParts.length != 3) {
      throw new Error("invalid token! must be a JWT");
    }
    const payload = parseJWTPart(tokenParts[1]);

    if(payload['brd:ct'] == 'acc') {
      throw new Error("token is an account token - a user or client token is required");
    } else if(payload['brd:ct'] == 'cli') {
      this.clientJWT = token;
    } else if(payload['brd:ct'] == 'usr') {
      this.userJWT = token;
    }

  }

  // function for signing (ECDSA/SHA256) with above keys:
  sign(string) {
    const hash = bitcore.crypto.Hash.sha256(Buffer.from(string,'utf-8'));
    return bitcore.crypto.ECDSA.sign(hash, this.privateKey);
  }

  async ensureUserJWT() {
    if(this.userJWT) return { result: this.userJWT };
    if(!this.clientJWT) {
      throw new Error("no client JWT");
    }

    if(this._userJWTLatch) return await this._userJWTLatch.promise();
    const latch = this._userJWTLatch = new PromiseLatch();

    try {
      const userTokenRequest = {
        "signature": this.sign(this.clientJWT).toDER().toString('base64'),
        "pub_key": this.publicKey.toDER().toString('base64'),
        "device_id": this.deviceId,
      };

      const { error, result, response: tokenResponse } = await this.net({
        method: 'POST',
        url: 'users/token',
        body: userTokenRequest,
        headers: { authorization: `Bearer ${this.clientJWT}` }
      });

      if(error) {
        console.error("Error authenticating: ",error);
        return { error };
      }

      const userToken = result.token;
      const clientId = result.client_token;

      const serverDate = tokenResponse.headers['x-date'] || tokenResponse.headers['date'];
      console.log("SERVER DATE DIFF: ",(new Date() - new Date(serverDate))/1000);
      
      if(!serverDate) {
        console.error("No server-date received");
      }

      const now = () => (new Date(serverDate)).getTime() / 1000;

      const jwtHeader = jwtEncode({ alg: 'ES256', typ: 'JWT' });

      const jwtObject = {
        sub: result.token,
        iat: now()-60, // <-- 60sec padding
        exp: now()+(12*60*60), // <-- 7 days token
        'brd:ct': 'usr',
        'brd:cli': clientId,
      };

      const jwtBody = jwtEncode(jwtObject);
      const jwtMain = `${jwtHeader}.${jwtBody}`;

      const jwtSignature = this.sign(jwtMain);
      const jwtSignatureString = ECDSASigFormatter.derToJose(jwtSignature.toDER(),'ES256');

      const userJWT = `${jwtMain}.${jwtSignatureString}`;
      this.userJWT = userJWT;
      latch.resolve({ result: this.userJWT });
    } catch(x) {
      latch.resolve({ error: x });
      return { error: x };
    } finally {
      this._userJWTLatch = null;
    }

    return { result: this.userJWT };
  }

  async middleware(inHash,next) {
    const hash = { ...inHash };
    hash.baseURL = hash.baseURL || this.apiBase || 'https://api.blockset.com/';

    const topic = hash.url.split(/^.*?(\w+)/)[1];

    if(USER_TOPICS.includes(topic)) {
      const { result, error } = await this.ensureUserJWT();
      if(error) return { error };

      hash.headers = { ...hash.headers };
      if(!hash.headers.authorization) {
        hash.headers.authorization = `Bearer ${this.userJWT}`;
      }
    }

    let res = await next(hash);

    if(res.error) {
      if(res.error.status == 403 && !hash.retried) {
        console.log("Got a 403, retrying with a fresh JWT...");
        
        delete this.userJWT;
        await this.ensureUserJWT();
        return this.net({ ...hash, retried: true });
      }
      
      // maybe deal with 403s by re-creating user token and re-issuing
      // request?
      console.error("Error from blockset: ",res.error);
      debugger;
    }

    return res;
  }
  
  net(options) {
    let hash;

    if(typeof options == 'string') {
      hash = { url: options };
    } else {
      hash = options;
    }
    
    const middleware = hash.middleware || unet.DEFAULT_MIDDLEWARE;

    return unet({
      ...hash,
      middleware: [this.middleware.bind(this),...middleware],
    });
  }
}

export function base64ToBase64URL(str) {
  return str.replace(/\+/g,'-').replace(/\//g,'_').replace(/\=/g,'');
}

function base64urlToBase64(str,padded=true) {
  const prePadded = str.replace(/\-/g,'+').replace(/_/g,'/');
  if(padded) {
    // pad to multiple of 3 with equals signs:
    return prePadded + '='.repeat((3-prePadded.length%3)%3);
  } else {
    return prePadded;
  }
}

function jwtEncode(object) {
  return base64ToBase64URL(Buffer.from(JSON.stringify(object)).toString('base64'));
}

function parseJWTPart(part) {
  try {
    return JSON.parse(atob(base64urlToBase64(part,false)));
  } catch(x) {
    return { error: x.toString() }
  }
}

const UUID = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
  var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
  return v.toString(16);
})

export default BlocksetAPI;
