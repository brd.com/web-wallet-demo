const { Deployment } = require('@brd.com/deploy-it');

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || 'debug';
const IMAGE = process.env.IMAGE || 'debug.docker/image:tag';

module.exports = () => {
  const D = new Deployment(ENV_SLUG);
  
  D.replicas(process.env.REPLICAS || 1)
  console.log("Replicas type: ",typeof D.get('spec.replicas'));
  console.log("Replicas: ",D.get('spec.replicas'));
  
  // is this actually better?
  
  D.template.container('app')
    .port('app',8080)
    .env('PORT',8080)
    .env('NODE_ENV','production')
    .image(IMAGE)
    .secretEnv('COOKIE_SECRET',{
      name: `${ENV_SLUG}-keys`,
      key: 'cookie-secret'
    });
  
  return D;
}
