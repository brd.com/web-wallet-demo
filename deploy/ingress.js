const { Ingress } = require('@brd.com/deploy-it');

const ENV_SLUG = process.env.CI_ENVIRONMENT_SLUG || 'debug';
const HOST = process.env.APP_HOST || 'test-host.co';
const PATH = process.env.APP_PATH || '/test-path';

module.exports = () => {
  const I  = new Ingress(ENV_SLUG);

  I.ingressClass('nginx');

  const host = I.host(`${HOST}`)
        .path(`${PATH}`,ENV_SLUG,8080);

  if(process.env.TLS_SECRET) {
    host.tls(process.env.TLS_SECRET);
  }

  return I;
};
