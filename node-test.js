import Wallet from './lib/wallet';
import WebhookCatcher from './lib/webhook-catcher';
import { pause } from './lib/utils';

async function time(p) {
  const a = new Date();
  let b;
  let error;
  
  try {
    await p;
  } catch(x) {
    error = x;
  } finally {
    b = new Date();
    if(error) {
      console.log(`TIME ELAPSED: ${((b-a)/1000).toFixed(3)}s`);
    }
  }

  if(error) throw error;

  return (b-a)/1000;
}

(async function() {
  let wallet,catcher;
  
  try {
    wallet = Wallet('upper high until jaguar under fish flash lift hazard penalty text trophy', { testnet: true, api: 'https://api.blockset.com/' });

    const syncDuration = await time(wallet.sync())
    const transactionCount = wallet.currencies.btc.transactions.length;
    
    console.log(`SYNCed ${transactionCount} in ${syncDuration}s`);

    const btc = wallet.currencies.btc;
    const deviceId = wallet.blockset.deviceId;
    console.log("Address: ",btc.receiveAddress);

    catcher = new WebhookCatcher(deviceId+'-'+btc.receiveAddress);
    
    const { error, result } = await wallet.blockset.net({
      method: 'POST',
      url: '/subscriptions',
      body: {
        "device_id": deviceId,
        "endpoint": {
          "environment": 'production',
          "kind": "webhook",
          "value": catcher.url,
        },
        "currencies": [
          {
            "addresses": [ btc.receiveAddress, ],
            "currency_id": "bitcoin-testnet:__native__",
            "events": [
              { "name": "submitted", "confirmations": [] },
              { "name": "confirmed", "confirmations": [1] }
            ]
          }
        ]
      }
    });

    if(error) {
      console.error("ERROR WITH SUBSCRIPTION: ",error);
      throw new Error("BlocksetError");
    }

    const sent = new Date();
    const sendDuration = await time(btc.send(btc.receiveAddress,1000,400));
    console.log(`Submitted transaction in ${sendDuration}s`);

    const webhook = await Promise.race([
      catcher.matching(({type,payload}) => true),
      pause(10000),
    ]);

    const gotWebhook = new Date();
    console.log("GOT WEBHOOK: ",webhook);
    console.log("Time from send to webhook receipt: ",((gotWebhook - sent)/1000).toFixed(3));
  } finally {
    if(catcher) catcher.close();
    wallet.$destroy();
  }
})();
