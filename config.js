const fs = require('fs');
const path = require('path');
const { deepMerge } = require('@brd.com/node-app/lib/utils');

var config = {
  baseUrl: process.env.BASE_URL,
  appPath: process.env.APP_PATH,
  appHost: process.env.APP_HOST,
}

const union = (...arr) => Array.prototype.concat.call(...arr).filter((o,i,a) => a.indexOf(o) === i);

const overridePath = path.resolve(__dirname,'config.override.js');
if(fs.existsSync(overridePath)) {
  var overrides = require(overridePath);
  config = deepMerge(config,overrides);
}

module.exports = config;

