const path = require('path');

var hash = {
  env: require('./config.js'),
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ]
  },

  render: {
    csp: {
      hashAlgorithm: 'sha256',
      policies: {
        'script-src': [
        ]
      }
    }
  },

  modules: [
    '@nuxtjs/sentry',
    '@brd.com/node-app/nuxt/module',
  ],

  sentry: {
    config: {
      environment: process.env.SENTRY_ENV || 'none',
    },
  },

  router: {
    base: process.env.APP_PATH,
  },

  plugins: [
    '~/plugins/element-ui.js',
  ],
  
  css: [
  ],
  build: {
    analyze: process.env.BUNDLE_ANALYZE=='true',
    extend(config, { isDev, isClient, isServer }) {

      

    },
  },
};

module.exports = hash;
